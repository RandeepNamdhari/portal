
@extends('Layouts.master')

@section('content')

   

	<div style="align-self:center;">

		 @if(session('error')??0)
   <div style="background:lightpink;padding: 8px;color:red; width: 100%;margin:10px auto;">

   	{{session('error')}}

   </div>
   @endif

    @if(session('message')??0)
   <div style="background:lightgreen;padding: 8px;color:darkgreen; width: 100%;margin:10px auto;">

   	{{session('message')}}
   	
   </div>
   @endif

		{!!Form::open(array('route'=>'login.store','method'=>'post','style'=>'display:flex;'))!!}

       <div style="display:flex;justify-content:flex-start;">

       	{!!Form::label('email', 'E-Mail Address');!!}

       	<div>

       		{!!Form::email('email',"",array('style'=>'width:323px;'))!!}

       		 @error('email')
       
            <div style="color:red;">{{ $message }}

            </div>

             @enderror

       	</div>
       	
       </div>

      
        {!!Form::submit('Sign Up',array('style'=>'max-height:22px;'))!!}
        
		{!!Form::close()!!}

	</div>

	@endsection


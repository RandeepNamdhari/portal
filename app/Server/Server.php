<?php

namespace App\Server;



use Illuminate\Support\Facades\Http;

use Illuminate\Http\Client\ConnectionException;


class Server 
{
    protected $server;
    protected $headers;

    function __construct()
    {

    	$this->server = env('API_SERVER');
    	$this->headers=[];

    }

    public function get(string $url)
    {
      
    	if($token=\Cache::get('user_token'))
    	{
            	$this->headers['Authorization']='Bearer '.$token;
    	}

    try{

        
        
       $response = Http::withHeaders($this->headers)
                       ->get($this->server.$url);



                       } catch (ConnectionException $e) {

        return (object)array('status'=>false,'error'=>'Failed to connect server.','message'=>'');
          
      }
         
              if(!$response->serverError() && !$response->clientError()) {
                  return (object)$response->json();
              }
             else
             {
              if($response->failed())
                $error='You have not access for this action.';
                
              
              return (object)array('status'=>false,'error'=>$error??'Please try agian later.','message'=>'');

             }
              

                      

    }

    public function post(string $url,$data)
    {
        
       
        if($token=\Cache::get('user_token'))
        {
           
           $this->headers['Authorization']='Bearer '.$token;
        }


      try {

        $response = Http::withHeaders($this->headers)
                      ->post($this->server.$url,$data);
                    
          
      } catch (ConnectionException $e) {

        return (object)array('status'=>false,'error'=>'Failed to connect server.','message'=>'');
          
      }
            

               if(!$response->serverError() &&  !$response->clientError()){
                
                return (object)$response->json();
             }
             else
             {

              if($response->failed())
                $error='You have not access for this action.';
                
              
              return (object)array('status'=>false,'error'=>$error??'Please try agian later.','message'=>'');

              }

                         
    }
}

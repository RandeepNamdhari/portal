<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class PortalUser extends GenericUser implements UserContract
{
    public function getAuthIdentifierName()
    {
      return $this->attributes['email'];
    }
    public function getAuthIdentifier()
    {
        
       return $this->attributes['id'];
    }
    public function getAuthPassword()
    {
        return $this->attributes['email'];
    }
    public function getRememberToken(){}
    public function setRememberToken($value){}
    public function getRememberTokenName(){}
}
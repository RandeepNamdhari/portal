<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server\Server;


class LoginController extends Controller
{
	function __construct(Server $server)
	{
		$this->server = $server;
	}

	public function index()
	{
		return view('login')->withTitle('Login');
	}

	public function store(Request $request)
	{

		$request->validate(['email'=>'required|email']);
		
    if (\Auth::attempt($request->only('email'))) {
    	dd(auth()->user());
       // return Redirect::to('/dashboard')->with('success', 'Hi '. $username .'! You have been successfully logged in.');
    } else {
       return redirect()->back()->with('error', 'Email Account is not valid.');
    }
	}

	public function verify(string $uuid)
	{
		if($uuid)
		{

            
			$response=$this->server->get('api/user/verify/'.$uuid);
            
			if($response->status)
			{
				//echo '<pre>';print_r($response);die;
				
			if (\Auth::attempt(array('email'=>$response->data['email']))) {


               $user=auth()->user();

               
              
               \Cache::forget('user_token');
                \Cache::forever("user_token", $user->token);
                

				return redirect()->route('user.profile');
    	
            } else {

            return redirect()->back()->with('error', 'Email Account is not valid or expired.');
                 
                 }
			

			}
			else
			{
				return redirect('/')->withError($response->error);
			}


		}
	}


}

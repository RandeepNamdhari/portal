<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server\Server;

class UserController extends Controller
{
    function __construct(Server $server)
    {
        $this->server = $server;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')->withTitle('Home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['email'=>'required|email']);
        
        try {

            $response=$this->server->post('api/user/register',$request->all());


           
                return redirect()->back()->withMessage($response->message??'')->withError($response->error);
            
            
        } catch (Exception $e) {

            return redirect()->back()->withError($e->getMessage);
            
        }
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       
        $user=auth()->user();
        $title='Update Profile';

        return view('profile',compact('user','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate(['email'=>'required|email']);

         try {

            $response=$this->server->post('api/user/update/'.$id,$request->all());

        
                return redirect()->back()->withMessage($response->message??'')->withError($response->error??'');
            
            
        } catch (Exception $e) {

            return redirect()->back()->withError($e->getMessage);
            
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    }

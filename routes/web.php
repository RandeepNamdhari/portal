<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Authenticated Routes
Route::group(['middleware'=>'auth'],function(){

Route::get('user/profile','UserController@show')->name('user.profile');

Route::put('user/update/{id}','UserController@update')->name('user.update');

});

//end

Route::get('login','UserController@index')->name('login');

Route::get('/','UserController@index')->name('home.index');

Route::post('account/request','UserController@store')->name('account.request');


Route::get('user/verify/{uuid}','LoginController@verify')->name('login.verify');


